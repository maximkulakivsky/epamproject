import React from "react";
// Header
import Header from "./components/Header"
// Components
import Slider from './components/Slider'
import Home from './components/Home' 
import RecyclingPoints from './components/RecyclingPoints'
import Lessons from './components/Lessons'
import Lesson from "./components/Lesson";
import PageNotFound from './components/PageNotFound'
import Footer from './components/Footer'
import AboutWebsite from './components/AboutWebsite'
import AboutZeroWaste from './components/AboutZeroWaste'
import AboutWasteRecycling from './components/AboutWasteRecycling'
// Router
import {Route, BrowserRouter as Router, Switch} from "react-router-dom";
// Lessons
import {lessons} from "./storage/lessons"



function App() {
  return (
    <div className="content">
      <div className="header-block">
        <Slider/>
      </div>
      <Router>
        <Header />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/RecyclingPoints" component={RecyclingPoints} />
          <Route path="/Lessons/:id" render={(props) => <Lesson lessons={lessons} {...props}/>} />
          <Route path="/Lessons" render={(props) => <Lessons lessons={lessons} {...props}/>}/>
          <Route path="/AboutWebsite" component={AboutWebsite}/>
          <Route path="/AboutZeroWaste" component={AboutZeroWaste}/>
          <Route path="/AboutWasteRecycling" component={AboutWasteRecycling}/>
          <Route path="*" component={PageNotFound} />
        </Switch>
      </Router>
      <Footer/>
    </div>
  )
}

export default App;
