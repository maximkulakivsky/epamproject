export const lessons = [
    {
        id: 1,
        name: "lesson 1",
        youtubeId: "npPR95sVh8Y",
        comments: [
            {
                id: 1,
                username: "user",
                text: "hello, i am user"
            },
            {
                id: 2,
                username: "user 2",
                text: "hi, i am user 2"
            },
        ]
    },
    {
        id: 2,
        name: "lesson 2",
        youtubeId: "cYEkxP5vIiA",
        comments: [
            {
                id: 1,
                username: "user",
                text: "I am user"
            },
            {
                id: 2,
                username: "user 2",
                text: "I am user 2"
            },
        ]
    },
    {
        id: 3,
        name: "lesson 3",
        youtubeId: "xkG4jSbfReQ",
        comments: [
            {
                id: 1,
                username: "user 3",
                text: "Great video"
            }
        ]
    },
    {
        id: 4,
        name: "lesson 4",
        youtubeId: "2I8Tjb4Fy-Q",
        comments: [
            {
                id: 1,
                username: "user",
                text: "hello, i am user"
            },
            {
                id: 2,
                username: "user 2",
                text: "hi, i am user 2"
            },
        ]
    },
    {
        id: 5,
        name: "lesson 5",
        youtubeId: "pF72px2R3Hg",
        comments: [
            {
                id: 1,
                username: "user",
                text: "I am user"
            },
            {
                id: 2,
                username: "user 2",
                text: "I am user 2"
            },
        ]
    },
    {
        id: 6,
        name: "lesson 6",
        youtubeId: "L2Rc8oTOtd8",
        comments: [
            {
                id: 1,
                username: "user 3",
                text: "Great video"
            }
        ]
    },
    {
        id: 7,
        name: "lesson 7",
        youtubeId: "14r7f9khK70",
        comments: [
            {
                id: 1,
                username: "user",
                text: "hello, i am user"
            },
            {
                id: 2,
                username: "user 2",
                text: "hi, i am user 2"
            },
        ]
    },
    {
        id: 8,
        name: "lesson 8",
        youtubeId: "vufLW4xOsS4",
        comments: [
            {
                id: 1,
                username: "user",
                text: "I am user"
            },
            {
                id: 2,
                username: "user 2",
                text: "I am user 2"
            },
        ]
    },
    {
        id: 9,
        name: "lesson 9",
        youtubeId: "oRQLilXLAIU",
        comments: [
            {
                id: 1,
                username: "user 3",
                text: "Great video"
            }
        ]
    },
    {
        id: 10,
        name: "lesson 10",
        youtubeId: "s4LZwCDaoQM",
        comments: [
            {
                id: 1,
                username: "user 3",
                text: "Great video"
            }
        ]
    }
]