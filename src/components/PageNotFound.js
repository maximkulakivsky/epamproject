import React from 'react'

class PageNotFound extends React.Component {

    render() {
        return (
            <div className="PageNotFound">
                <div>404</div>
            </div>
        )
    }
}

export default PageNotFound
