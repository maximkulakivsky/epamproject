import React, {useState, useEffect } from "react";
import { withScriptjs, withGoogleMap, GoogleMap, Marker } from "react-google-maps"
import {Button} from 'reactstrap'



function RecyclingPoints(props)
{
    let map = React.createRef();

    const [coordsList, setCoordsList] = React.useState([]);
    const [centerLat, setCenterLat] = React.useState(50.45);
    const [centerLng, setCenterLng] = React.useState(30.5);

    function loadPlaces(e)
    {
        let lat = map.current.getCenter().lat();
        let lng = map.current.getCenter().lng();
        const key = "AIzaSyA81reuSrqMQKghDk6ytLHY8F1FTHDLKIg";
        const radius = 10000;
        const keyword = "Прием вторсырья";
        const language = "ru";
        let request = `https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=${key}&location=${lat},${lng}&radius=${radius}&keyword=${keyword}&language=${language}`
        
        let xhr = new XMLHttpRequest();
        xhr.open('GET', `https://cors-anywhere.herokuapp.com/${request}`, true);
        xhr.send();
        xhr.onload = function() {
            if (xhr.status === 200)
            {
                let places = []
                JSON.parse(xhr.response).results.forEach(place => {
                    let coords = place.geometry.location;
                    places.push(coords);
                });
                setCenterLat(lat);
                setCenterLng(lng);
                setCoordsList(places);
            }
        }
    }

    const MyMapComponent = withScriptjs(withGoogleMap((props) =>
        <GoogleMap
            ref={map}
            defaultZoom={12}
            defaultCenter={{lat: centerLat, lng: centerLng}}
        >
            {
                props.isMarkerShown ? 
                coordsList.map( (coord, i) => {
                    return <Marker position={{ lat: coord.lat, lng: coord.lng }} /> 
                })
                : {}
            }
        </GoogleMap>
    ))

    return (
        <div className="RecyclingPoints">
            <MyMapComponent
                isMarkerShown
                googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyA81reuSrqMQKghDk6ytLHY8F1FTHDLKIg&libraries=places,geometry"
                loadingElement={<div style={{ height: `100%` }} />}
                containerElement={<div style={{ height: `600px` }} />}
                mapElement={<div style={{ height: `100%` }} />}
            />
            <Button color="info" onClick={loadPlaces} style={{marginTop:"5px", width:"100%", marginBottom:"30px"}}>Load Recycling points</Button>
        </div>
    );
    
}

export default RecyclingPoints
