import React from "react";
// Slider
import AwesomeSlider from 'react-awesome-slider';
import AwesomeSliderStyles from 'react-awesome-slider/src/styles.js';
import withAutoplay from 'react-awesome-slider/dist/autoplay';
import img1 from '../storage/img/img1.jpg'
import img2 from '../storage/img/img2.jpg'
import img3 from '../storage/img/img3.jpg'
import img4 from '../storage/img/img4.jpg'
import img5 from '../storage/img/img5.jpg'

const AutoplaySlider  = withAutoplay(AwesomeSlider);

function Slider(props)
{
    return (
        <AutoplaySlider 
            style={{width: "100%", minWidth: "400px", height: "200px"}}
            cssModule={AwesomeSliderStyles}
            bullets={false}
            organicArrows={false} 
            play={true}
            cancelOnInteraction={false}
            interval={3000}
        >
            <div data-src={img1}/>
            <div data-src={img2}/>
            <div data-src={img3}/>
            <div data-src={img4}/>
            <div data-src={img5}/>
        </AutoplaySlider >
    )
}

export default Slider;