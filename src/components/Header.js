import React, { useState } from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink, UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem, NavbarText } from 'reactstrap';
import { Link } from 'react-router-dom';

function Header(props){
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div>
      <Navbar expand="md">
        <NavbarBrand><Link className="link" to="/">Home</Link></NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              <NavLink><Link className="link" to="/RecyclingPoints">Recycling Points</Link></NavLink>
            </NavItem>
            <NavItem>
              <NavLink><Link className="link" to="/Lessons">Lessons</Link></NavLink>
            </NavItem>
            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret>
                <Link className="link" to="/AboutWebsite" >About</Link>
              </DropdownToggle>
              <DropdownMenu right>
                <DropdownItem>
                    <Link className="link link-dropdown" to="/AboutZeroWaste">About Zero Waste</Link>
                </DropdownItem>
                <DropdownItem>
                    <Link className="link link-dropdown" to="/AboutWasteRecycling">About Waste Recycling</Link>
                </DropdownItem>
                <DropdownItem divider />
                <DropdownItem>
                    <Link className="link link-dropdown" to="/AboutWebsite">About Web Site</Link>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav>
          <NavbarText>Zero Waste</NavbarText>
        </Collapse>
      </Navbar>
    </div>
  );
}

export default Header;