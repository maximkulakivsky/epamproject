import React from 'react'

class Home extends React.Component {
    render() {
        return (
            <div>
                <h2>Zero waste</h2>
                <p>
                    Zero Waste is a set of principles focused on waste prevention that encourages the redesign of resource life cycles so that all products are reused. The goal is for no trash to be sent to landfills, incinerators, or the ocean. Currently, only 9% of plastic is actually recycled. In a zero waste system, material will be reused until the optimum level of consumption. The definition adopted by the Zero Waste International Alliance (ZWIA) is:
                </p>
                <p>
                    Zero Waste: The conservation of all resources by means of responsible production, consumption, reuse, and recovery of all products, packaging, and materials, without burning them, and without discharges to land, water, or air that threaten the environment or human health.
                </p>
                <p>
                    Zero Waste refers to waste prevention as opposed to end-of-pipe waste management. It is a whole systems approach that aims for a massive change in the way materials flow through society, resulting in no waste. Zero waste encompasses more than eliminating waste through recycling and reuse, it focuses on restructuring production and distribution systems to reduce waste. Zero waste is more of a goal or ideal rather than a hard target. Zero Waste provides guiding principles for continually working towards eliminating wastes.
                </p>
                <p>
                    Advocates expect that government regulation is needed to influence industrial choices over product and packaging design, manufacturing processes, and material selection.
                </p>
                <p>
                    Advocates say eliminating waste decreases pollution, and can also reduce costs due to the reduced need for raw materials.
                </p>
                <h2>Recycling</h2>
                <p>
                    Recycling is the process of converting waste materials into new materials and objects. The recyclability of a material depends on its ability to reacquire the properties it had in its virgin state. It is an alternative to "conventional" waste disposal that can save material and help lower greenhouse gas emissions. Recycling can prevent the waste of potentially useful materials and reduce the consumption of fresh raw materials, thereby reducing: energy usage, air pollution (from incineration), and water pollution (from landfilling).
                </p>
                <p>
                    Recyclable materials include many kinds of glass, paper, cardboard, metal, plastic, tires, textiles, batteries, and electronics. The composting or other reuse of biodegradable waste—such as food or garden waste—is also a form of recycling. Materials to be recycled are either delivered to a household recycling center or picked up from curbside bins, then sorted, cleaned, and reprocessed into new materials destined for manufacturing new products.
                </p>
            </div>
        );
    }
}

export default Home
