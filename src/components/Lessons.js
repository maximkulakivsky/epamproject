import React, {useState} from "react";
import {
    Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button
  } from 'reactstrap';
import {Link} from "react-router-dom"
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';

let page = 0;

function Lessons({lessons}) {
    const pageCapacity = 3;
    const length = lessons.length;
    const pagesAmount = Math.ceil(length / pageCapacity);
    const [showingLessons, setShowingLessons] = useState(lessons.slice(page * pageCapacity, (page + 1) * pageCapacity));
    function paginationLeft() {
        if (page > 0)
        {
            page--;
            setShowingLessons(lessons.slice(page * pageCapacity, (page + 1) * pageCapacity));
        }
    }

    function paginationRight() {
        if (page < pagesAmount - 1)
        {
            page++;
            setShowingLessons(lessons.slice(page * pageCapacity, (page + 1) * pageCapacity));
        }
    }
        
    return (
        <div className="lessons-block">
            <div className="lessons-list">
            {
                showingLessons.map( (lesson) => {
                    return (
                        <div>
                            <Card className="lesson">
                                <CardImg top width="240px" height="180px" src={`https://i1.ytimg.com/vi/${lesson.youtubeId}/0.jpg`} alt="Card image cap" />
                                <CardBody>
                                <CardTitle><Link to={"/Lessons/" + lesson.id}>{lesson.name}</Link></CardTitle>
                                <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                                </CardBody>
                            </Card>
                        </div>
                    );
                })
            }
            </div>
            <Pagination className="pagination">
            <PaginationItem onClick={paginationLeft}>
                <PaginationLink>{`<<`}</PaginationLink>
            </PaginationItem>
            <PaginationItem>
                <PaginationLink>{page + 1}/{pagesAmount}</PaginationLink>
            </PaginationItem>
            <PaginationItem onClick={paginationRight}>
                <PaginationLink>{`>>`}</PaginationLink>
            </PaginationItem>
            </Pagination>
        </div>
    )
}
  
export default Lessons;