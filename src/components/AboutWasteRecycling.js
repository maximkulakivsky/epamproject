import React from 'react'

class AboutWasteRecycling extends React.Component {
    render() {
        return (
            <div>
                <h2>Recycling</h2>
                <p>
                    Recycling is the process of converting waste materials into new materials and objects. The recyclability of a material depends on its ability to reacquire the properties it had in its virgin state. It is an alternative to "conventional" waste disposal that can save material and help lower greenhouse gas emissions. Recycling can prevent the waste of potentially useful materials and reduce the consumption of fresh raw materials, thereby reducing: energy usage, air pollution (from incineration), and water pollution (from landfilling).
                </p>
                <p>
                    Recyclable materials include many kinds of glass, paper, cardboard, metal, plastic, tires, textiles, batteries, and electronics. The composting or other reuse of biodegradable waste—such as food or garden waste—is also a form of recycling. Materials to be recycled are either delivered to a household recycling center or picked up from curbside bins, then sorted, cleaned, and reprocessed into new materials destined for manufacturing new products.
                </p>
            </div>
        );
    }
}

export default AboutWasteRecycling
