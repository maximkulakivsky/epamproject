import React, {useState} from "react";
import {useParams} from "react-router-dom"
import { Card, CardText, CardBody, CardTitle } from 'reactstrap';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';




function Lesson(props) {
    let { id } = useParams();
    let lessons = props.lessons;
    let lessonIndex = lessons.findIndex((less) => {return less.id == id});
    let lesson = lessons[lessonIndex];

    function AddComment(e) {
        e.preventDefault();
        let username = e.target["username"].value
        let comment = e.target["comment"].value;
        lessons[lessonIndex].comments.push({
            id: Date.now(),
            username: username,
            text: comment
        })
        props.history.push(`/Lessons/${id}`)
    }

    return (
        <div>
            <iframe 
            width="100%" 
            height="600px" 
            src={`https://www.youtube.com/embed/${lesson.youtubeId}`} 
            frameborder="0" 
            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
            </iframe>
            {
                lesson.comments.map((comment) => {
                    return (
                        <Card style={{marginTop:"5px", border:"1px solid black"}}>
                            <CardBody>
                                <CardTitle><b>{comment.username}</b></CardTitle>
                                <CardText>{comment.text}</CardText>
                            </CardBody>
                        </Card>
                    )
                })
            }
            <br></br>
            <Form onSubmit={AddComment}>
                <FormGroup>
                    <Label for="username">Your name:</Label>
                    <Input type="text" name="username" id="username"/>
                </FormGroup>
                <FormGroup>
                    <Label for="comment">Your comment:</Label>
                    <Input type="textarea" name="comment" id="comment"/>
                </FormGroup>
                <Button style={{width:"100%"}} type="submit">Comment</Button>
            </Form>
            <br></br>
            <br></br>
        </div>
    )
}
  
export default Lesson;